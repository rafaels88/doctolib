class Event < ActiveRecord::Base
  def self.availabilities(start_date)
    availability_service.available_times(from: start_date)
  end

  def self.events_in(date)
    today = where(starts_at: date..date.end_of_day)
    with_recurrence = where("strftime('%w', starts_at) = '#{date.wday}'")
                      .where(weekly_recurring: true)
                      .where('starts_at < ?', date)

    today + with_recurrence
  end

  def self.availability_service
    Events::AvailabilityService.new(event_repository: self)
  end

  def opening?
    kind == 'opening'
  end

  def appointment?
    kind == 'appointment'
  end
end
