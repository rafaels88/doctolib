module Events
  class AvailabilityService
    attr_reader :repository

    def initialize(event_repository:)
      @repository = event_repository
    end

    def available_times(from:, days: 7)
      to = future_date(from, days: days)

      (from..to).map do |date|
        slots = generate_slots_for(repository.events_in(date))
        {
          date: date,
          slots: slots[:opened] - slots[:appointment]
        }
      end
    end

    private

    def generate_slots_for(events)
      slots_table = {
        appointment: [],
        opened: []
      }

      events.each do |event|
        slots = thirty_minutes_slots_for(event)

        if event.opening?
          slots_table[:opened] += slots

        elsif event.appointment?
          slots_table[:appointment] += slots
        end
      end

      slots_table
    end

    def thirty_minutes_slots_for(event)
      slots = []
      (event.starts_at.to_i..event.ends_at.to_i).step(30.minute) do |d|
        slots.push(Time.zone.at(d).strftime("%-H:%M"))
      end
      slots[0...-1]
    end

    def future_date(date, days:)
      (date + (days - 1).day).end_of_day
    end
  end
end
