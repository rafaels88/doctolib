require 'test_helper'

class EventTest < ActiveSupport::TestCase
  test 'does not return free time when an appointment exists in that time' do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse('2014-08-11 09:30'),
                 ends_at: DateTime.parse('2014-08-11 12:30'),
                 weekly_recurring: true
    Event.create kind: 'appointment',
                 starts_at: DateTime.parse('2014-08-11 10:30'),
                 ends_at: DateTime.parse('2014-08-11 11:30')

    availabilities = Event.availabilities DateTime.parse('2014-08-10')
    assert_equal Date.new(2014, 8, 10), availabilities[0][:date]
    assert_equal [], availabilities[0][:slots]
    assert_equal Date.new(2014, 8, 11), availabilities[1][:date]
    assert_equal ['9:30', '10:00', '11:30', '12:00'], availabilities[1][:slots]
    assert_equal Date.new(2014, 8, 16), availabilities[6][:date]
    assert_equal 7, availabilities.length
  end

  test 'does not return free time for recurrent open event when there is ' \
    'an appointment in the given date' do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse('2014-08-04 09:30'),
                 ends_at: DateTime.parse('2014-08-04 12:30'),
                 weekly_recurring: true
    Event.create kind: 'appointment',
                 starts_at: DateTime.parse('2014-08-11 10:30'),
                 ends_at: DateTime.parse('2014-08-11 11:30')

    availabilities = Event.availabilities DateTime.parse('2014-08-10')
    assert_equal Date.new(2014, 8, 10), availabilities[0][:date]
    assert_equal [], availabilities[0][:slots]
    assert_equal Date.new(2014, 8, 11), availabilities[1][:date]
    assert_equal ['9:30', '10:00', '11:30', '12:00'], availabilities[1][:slots]
    assert_equal Date.new(2014, 8, 16), availabilities[6][:date]
    assert_equal 7, availabilities.length
  end

  test 'returns empty slots when no opening events is found' do
    availabilities = Event.availabilities DateTime.parse('2014-08-10')
    start_date = DateTime.new(2014, 8, 10)
    end_date = DateTime.new(2014, 8, 16)

    (start_date..end_date).each_with_index do |date, i|
      assert_equal date, availabilities[i][:date]
      assert_equal [], availabilities[i][:slots]
    end
    assert_equal 7, availabilities.length
  end

  test 'returns all available slots when for two days of opening events' do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse('2014-08-04 09:30'),
                 ends_at: DateTime.parse('2014-08-04 12:30'),
                 weekly_recurring: true
    Event.create kind: 'opening',
                 starts_at: DateTime.parse('2014-08-05 09:30'),
                 ends_at: DateTime.parse('2014-08-05 12:30'),
                 weekly_recurring: true
    Event.create kind: 'appointment',
                 starts_at: DateTime.parse('2014-08-11 10:30'),
                 ends_at: DateTime.parse('2014-08-11 11:30')

    availabilities = Event.availabilities DateTime.parse('2014-08-10')
    assert_equal Date.new(2014, 8, 10), availabilities[0][:date]
    assert_equal [], availabilities[0][:slots]
    assert_equal Date.new(2014, 8, 11), availabilities[1][:date]
    assert_equal ['9:30', '10:00', '11:30', '12:00'], availabilities[1][:slots]
    assert_equal Date.new(2014, 8, 12), availabilities[2][:date]
    assert_equal ['9:30', '10:00', '10:30', '11:00', '11:30', '12:00'],
                 availabilities[2][:slots]
    assert_equal Date.new(2014, 8, 16), availabilities[6][:date]
    assert_equal 7, availabilities.length
  end

  test 'does not return past opening event with it is not recurrent' do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse('2014-08-04 09:30'),
                 ends_at: DateTime.parse('2014-08-04 12:30'),
                 weekly_recurring: true
    Event.create kind: 'opening',
                 starts_at: DateTime.parse('2014-08-05 09:30'),
                 ends_at: DateTime.parse('2014-08-05 12:30'),
                 weekly_recurring: false
    Event.create kind: 'appointment',
                 starts_at: DateTime.parse('2014-08-11 10:30'),
                 ends_at: DateTime.parse('2014-08-11 11:30')

    availabilities = Event.availabilities DateTime.parse('2014-08-10')
    assert_equal Date.new(2014, 8, 10), availabilities[0][:date]
    assert_equal [], availabilities[0][:slots]
    assert_equal Date.new(2014, 8, 11), availabilities[1][:date]
    assert_equal ['9:30', '10:00', '11:30', '12:00'], availabilities[1][:slots]
    assert_equal Date.new(2014, 8, 12), availabilities[2][:date]
    assert_equal [], availabilities[2][:slots]
    assert_equal Date.new(2014, 8, 16), availabilities[6][:date]
    assert_equal 7, availabilities.length
  end
end
